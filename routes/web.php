<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\AdminPostController;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\PostController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PaymentController;

Route::get('/', [PostController::class, 'index'])->name('post.index');
Route::get('/post/{post}', [PostController::class, 'show'])->name('post.show');

Route::get('/post/search', [PostController::class, 'search'])->name('post.search');

Auth::routes(['verify' => true]);

Route::middleware('guest')->group(function () {
    Route::get('/register', [AuthController::class, 'indexRegister'])->name('indexRegister');
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::get('/login', [AuthController::class, 'index'])->name('login');
    Route::post('/login', [AuthController::class, 'login'])->name('customLogin');

});

Route::post('/logout', function () {
    Auth::logout();
    return redirect('/')->with('logoutMsg', 'Logout Successfully');
})->name('logout');

Route::get('/home', function () {
    return redirect('/');
});


Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/user/create', [PostController::class, 'showCreateForm'])->name('post.create')->middleware('subscription');
    Route::post('/user/create', [PostController::class, 'store'])->name('post.store')->middleware('subscription');
    Route::get('/user/posts/', [PostController::class, 'singleUserPosts'])->name('user.posts');
    Route::get('/user/posts/{id}', [PostController::class, 'edit'])->name('user.posts.edit');
    Route::put('/user/posts/{id}', [PostController::class, 'update'])->name('user.posts.update');
    Route::delete('/user/posts/{id}', [PostController::class, 'destroy'])->name('user.posts.delete');

    Route::get('posttable', [PostController::class, 'tableView'])->name('post.table');
    Route::get('posttable/sort-ascend', [PostController::class, 'sortByAscendTable'])->name('post.sort.ascend');
    Route::get('posttable/sort-older', [PostController::class, 'sortByOlderTable'])->name('post.sort.older');

    Route::get('/subscription/', [PaymentController::class, 'index'])->name('payment.index');
    Route::get('/subscription/silver', [PaymentController::class, 'silverSubscription'])->name('payment.silver');
    Route::get('/subscription/diamond', [PaymentController::class, 'diamondSubscription'])->name('payment.diamond');
    Route::get('/paypal-checkout/{price}', [PaymentController::class, 'paypalCheckout'])->name('paypal.checkout');

    Route::post('posts/{post}/like', [LikeController::class, 'store'])->name('posts.likes');
    Route::delete('posts/{post}/like', [LikeController::class, 'destroy'])->name('posts.likes');

    Route::post('comment/store/{post}',[CommentController::class,'store'])->name('comment.store');

});

Route::prefix('admin', 'verified')->name('admin.')->middleware('can:manage-users')->group(function () {
    Route::resource('/users', UserController::class)->except(['show', 'create', 'store']);
    Route::get('/posts/', [AdminPostController::class, 'index'])->name('posts.table');
    Route::delete('/posts/{id}', [AdminPostController::class, 'destroy'])->name('posts.delete');
    Route::get('/posts/{id}', [AdminPostController::class, 'edit'])->name('posts.edit');
    Route::put('/posts/{id}', [AdminPostController::class, 'update'])->name('posts.update');

    Route::get('posttable/sort-ascend', [AdminPostController::class, 'sortByAscendTable'])->name('post.sort.ascend');
    Route::get('posttable/sort-older', [AdminPostController::class, 'sortByOlderTable'])->name('post.sort.older');
    Route::get('posttable/sort-active', [AdminPostController::class, 'showActivePost'])->name('post.sort.active');
    Route::get('posttable/sort-notactive', [AdminPostController::class, 'showNotActivePost'])->name('post.sort.notactive');


    Route::post('/import', [UserController::class, 'import'])->name("user.import");
    Route::get('/export', [UserController::class, 'export'])->name("user.export");
});
