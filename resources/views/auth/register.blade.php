@extends('layouts.app')

@section('content')
    <header>
        <div class="h-screen bg-gray-100 flex justify-center">


            <div class="py-6 px-8  mt-20 bg-white rounded shadow-xl ">
                @if (session('status'))
                    <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                        {{session('status')}}
                    </div>
                @endif
                <form action="{{route('register')}}" method="post">
                    @csrf
                    <div class="mb-6">
                        <label for="name" class="block text-gray-800 font-bold">Name:</label>
                        <input type="text" required name="name" placeholder="name" class="w-full border border-gray-300 py-2 pl-3 rounded mt-2 outline-none focus:ring-indigo-600 :ring-indigo-600" />
                    </div>

                    <div>
                        <label for="email" class="block text-gray-800 font-bold">Email:</label>
                        <input type="email" required name="email" id="email" placeholder="@email" class="w-full border border-gray-300 py-2 pl-3 rounded mt-2 outline-none focus:ring-indigo-600 :ring-indigo-600" />

                    </div>
                    <div class="mb-6">
                        <label for="password" class="block text-gray-800 font-bold">Name:</label>
                        <input type="password" required name="password"  placeholder="password" class="w-full border border-gray-300 py-2 pl-3 rounded mt-2 outline-none focus:ring-indigo-600 :ring-indigo-600" />
                    </div>

                    <div class="mb-6">
                        <label for="password" class="block text-gray-800 font-bold">Confirm password:</label>
                        <input type="password" required name="password_confirmation" placeholder="Confirm password" class="w-full border border-gray-300 py-2 pl-3 rounded mt-2 outline-none focus:ring-indigo-600 :ring-indigo-600" />
                    </div>
                    <button type="submit" class="cursor-pointer py-2 px-4 block mt-6 bg-indigo-500 text-white font-bold w-full text-center rounded">Register</button>
                </form>
            </div>
        </div>
    </header>
@endsection
