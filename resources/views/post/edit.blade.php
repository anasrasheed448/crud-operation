@extends('layouts.app')

@section('content')

    <div class="w-6/12 bg-white p-6 rounded-lg">

        <form action="{{ route('user.posts.update',$post->id) }}" method="post"  class="mb-4" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="mb-6">
                <label for="Title" class="block text-gray-800 font-bold">Title:</label>
                <input type="text" name="title" value="{{$post->title}}" class="w-full border border-gray-300 py-2 pl-3 rounded mt-2 outline-none focus:ring-indigo-600 :ring-indigo-600" />
                @error('title')
                <div class="text-red-500 mt-2 text-sm mb-2">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <div class="mb-6">
                <label for="Choose Image" class="block text-gray-800 font-bold">Choose Image:</label>
                <img src="{{asset('images/'.$post->image)}}" height="70" width="100">
                <input type="file" name="image"  class="w-full border border-gray-300 py-2 pl-3 rounded mt-2 outline-none focus:ring-indigo-600 :ring-indigo-600" />
                @error('image')
                <div class="text-red-500 mt-2 text-sm mb-2">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="mb-6">
                <label for="Description" class="block text-gray-800 font-bold">Description:</label>
                <textarea name="description" id="Description" cols="30" rows="4"
                          class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('description') border-red-500 @enderror">{{$post->description}}
             </textarea>
            </div>
            @error('description')
            <div class="text-red-500 mt-2 text-sm mb-2">
                {{ $message }}
            </div>
            @enderror
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3 rounded font-medium">Post</button>

            </div>
        </form>


    </div>

@endsection
