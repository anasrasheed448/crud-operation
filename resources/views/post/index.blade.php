@extends('layouts.app')

@section('content')
    @if (Session::has('logoutMsg'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Succeed</strong> {{ Session::get('logoutMsg') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('post-approval'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Succeed</strong> {{ Session::get('post-approval') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <!-- component -->
    <div class="container my-12 mx-auto px-4 md:px-12">

        <div class="mt-3.5 flex flex-wrap -mx-1 lg:-mx-4">
            <!-- component -->
            <!-- This is an example component -->
            @if ($posts->count())
                @foreach ($posts as $post)
                    <!-- Column -->
                    <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">

                        <!-- Article -->

                        <article class="overflow-hidden rounded-lg shadow-lg">
                            <img alt="Placeholder" class="block w-full h-60" src="{{ asset('images/' . $post->image) }}">
                            <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                                <a href="{{ route('post.show', $post) }}">
                                    <h1 class="text-lg">
                                        {{ $post->title }}
                                    </h1>
                                </a>

                                <p class="text-grey-darker text-sm">
                                    {{ $post->created_at->diffForHumans() }}
                                </p>
                            </header>
                            <div class="flex ml-2">
                                @auth
                                    @if (!$post->likedBy(auth()->user()))
                                        <form method="post" action="{{ route('posts.likes', $post) }}">
                                            @csrf
                                            <button class="ml-2 mr-2  text-blue-500" type="submit">Like</button>
                                        </form>
                                    @else
                                        <form method="post" action="{{ route('posts.likes', $post) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="ml-2 mr-2 text-blue-500" type="submit">Unlike</button>
                                        </form>
                                    @endif
                                @endauth
                                <p class="font-bold text-md">{{ $post->likes->count() }}
                                    {{ Str::plural('like', $post->likes->count()) }}</p>
                            </div>
                            <div class="flex float-right text-right mr-2 ">Post By: <p class="text-base font-bold ml-1">
                                    {{ $post->user->name }}</p>
                            </div>
                            <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                                <p class="ml-2 text-sm">
                                    {{ $post->description }}
                                </p>
                            </footer>
                        </article>

                        <!-- END Article -->

                    </div>
                    <!-- END Column -->

                @endforeach




        </div>
        {{ $posts->links() }}
    </div>
@else
    <h1 class="flex m-auto text-xl"> No Post..</h1>
    @endif
@endsection
