@extends('layouts.app')

@section('content')


    <!-- component -->
    <div class="w-full my-12 mx-auto px-4 ">

        <div class=" ">
            <div class="my-1 px-1 lg:w-1/2 ">
                <article class="overflow-hidden rounded-lg shadow-lg">
                    <img alt="Placeholder" class="block w-full h-80" src="{{ asset('images/' . $post->image) }}">
                    <header class="flex items-center justify-between leading-tight p-2 md:p-4">
                        <h1 class="text-lg">
                            {{ $post->title }}
                        </h1>
                        <p class="text-grey-darker text-sm">
                            {{ $post->created_at->diffForHumans() }}
                        </p>

                    </header>
                    <div class="flex float-right text-right mr-2 ">Post By: <p class="text-base font-bold ml-1">
                            {{ $post->user->name }}</p>
                    </div>
                    <footer class="flex items-center justify-between leading-none p-2 md:p-4">
                        <p class="ml-2 text-sm">
                            {{ $post->description }}
                        </p>
                    </footer>

                    <h4 class="text-center mb-4 pt-20 pb-2">Nested comments section</h4>
                    <div class="row ">
                        <div class="col">
                            @forelse ($post->comments as $comment)
                                <div class="d-flex flex-start mt-4 ml-4">
                                    <a class="me-3" href="#">
                                        <img class="rounded-circle shadow-1-strong"
                                            src="https://mdbootstrap.com/img/Photos/Avatars/img%20(11).jpg" alt="avatar"
                                            width="65" height="65" />
                                    </a>
                                    <div class="flex-grow-1 flex-shrink-1">
                                        <div>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <p class="mb-1">
                                                    {{ $comment->username }}
                                                    <span class="small">{{ $comment->created_at->diffForHumans() }}</span>
                                                </p>
                                                <a href="#comment-form"
                                                    onclick=" document.getElementById('parent_id').value={{ $comment->id }};  document.getElementById('body').focus(); document.getElementById('body').placeholder='Enter Reply'; return false;"
                                                    class="mr-6"><i class="fa fa-reply fa-xs"></i><span class="small">
                                                        reply</span></a>
                                            </div>
                                            <p class="ml-2 mb-0">
                                                {{ $comment->body }}
                                            </p>
                                        </div>
                                        @foreach ($comment->replies as $reply)

                                            <div class="d-flex flex-start mt-4">
                                                <a class="me-3" href="">
                                                    <img class="rounded-circle shadow-1-strong"
                                                        src="https://mdbootstrap.com/img/Photos/Avatars/img%20(11).jpg"
                                                        alt="avatar" width="65" height="65" />
                                                </a>

                                                <div class="flex-grow-1 flex-shrink-1">
                                                    <div>
                                                        <div class="d-flex justify-content-between align-items-center">
                                                            <p class="mb-1">
                                                                {{ $reply->username }} <span class="small">-
                                                                    {{ $reply->created_at->diffForHumans() }}</span>
                                                            </p>
                                                        </div>
                                                        <p class="small mb-0">
                                                            {{ $reply->body }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            @empty

                            @endforelse

                            <div class="col-md-6 mt-10 ml-4" id="comment-form">
                                <form class="form-group" action="{{ route('comment.store', $post) }}" method="POST">
                                    @csrf
                                    <input id="body" placeholder="Enter Comment" class="form-control" style="background-color: rgb(235, 238, 232)"
                                        type="text" name="body">
                                    <input type="hidden" name="parent_id" id="parent_id" value="">
                                    <button class="btn btn-success mt-2" type="submit">Comment</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </article>
                <!-- END Article -->
            </div>
            <!-- END Column -->
        </div>

    </div>
@endsection

{{-- <script>
    $(document).ready(function() {
        var body = document.getElementById('body');

        function saveData(post) {
            $.$.ajax({
                type: "POST",
                url: "{{ route('comment.store', "+ post +") }}",
                data:  {
                    body : body
                },
                dataType: "dataType",
                success: function(response) {
                    alert('comment added')
                }
            });


        }
    });
</script> --}}
