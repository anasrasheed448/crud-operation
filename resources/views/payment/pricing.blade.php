@extends('layouts.app')
@section('content')
    @if (Session::has('subscription'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Succeed</strong> {{ Session::get('subscription') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="demo">
        <div class="container">
            @if (auth()->user()->hasRole('user'))
                <div class="col-md-3 col-sm-6 py-10">
                    <div class="pricingTable text-left">
                        <h3 class="duration text-black font-bold "> Current Subscription :-
                            {{ auth()->user()->payment->subscription_type }}
                        </h3>
                        <h3 class="duration text-black font-bold"> Expiry Date :-
                            {{ auth()->user()->payment->expiry_date }}
                        </h3>
                        <h3 class="duration text-black font-bold">
                            Post Left :- {{ auth()->user()->payment->post_count }}
                        </h3>
                    </div>
                </div>
            @endif

            <div class="row" style="margin-top: 20px">
                <div class="col-md-4 col-sm-6">
                    <a href="{{ route('paypal.checkout', '15') }}">
                        <div class="pricingTable blue">
                            <div class="pricingTable-header">
                                <div class="price-value">
                                    <span class="amount">$15</span>
                                    <span class="duration">per month</span>
                                </div>
                                <h3 class="title">Silver</h3>
                            </div>
                            <ul class="pricing-content">
                                <li>15 Post</li>
                            </ul>
                            <div id="paypal-button-container1"></div>

                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="{{ route('paypal.checkout', '30') }}">
                        <div class="pricingTable blue">
                            <div class="pricingTable-header">
                                <div class="price-value">
                                    <span class="amount">$30</span>
                                    <span class="duration">per month</span>
                                </div>
                                <h3 class="title">Dimond</h3>
                            </div>
                            <ul class="pricing-content">
                                <li>100 Posts</li>
                            </ul>


                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>


    <style>
        .pricingTable {
            color: #666;
            background: #fff;
            font-family: 'Poppins', sans-serif;
            text-align: center;
            padding: 25px;
            margin: 0 15px;
            border-radius: 40px;
            box-shadow: 5px 5px rgba(0, 0, 0, 0.1), 0 2px 20px rgba(0, 0, 0, 0.1) inset;
            position: relative;
        }

        .pricingTable .pricingTable-header {
            margin: 0 0 20px;
        }

        .pricingTable .price-value {
            margin: 0 0 20px;
        }

        .pricingTable .price-value .amount {
            font-size: 75px;
            line-height: 70px;
            font-weight: 400;
            margin: 0 0 8px;
            display: block;
        }

        .pricingTable .price-value .duration {
            font-size: 20px;
            line-height: 20px;
            text-transform: capitalize;
            font-weight: 400;
            display: block;
        }

        .pricingTable .title {
            color: #fff;
            background-color: #E96134;
            font-size: 28px;
            font-weight: 600;
            text-transform: uppercase;
            letter-spacing: 2px;
            padding: 10px;
            margin: 0;
            border-radius: 50px;
            box-shadow: 0 -5px 7px rgba(0, 0, 0, 0.25) inset, 0 0 10px rgba(0, 0, 0, 0.2);
        }

        .pricingTable .pricing-content {
            background-color: #E6E8EA;
            padding: 10px;
            margin: 0 0 10px;
            list-style: none;
            border-radius: 20px;
            box-shadow: 0 -5px 7px rgba(0, 0, 0, 0.15) inset, 0 0 10px rgba(0, 0, 0, 0.15);
        }

        .pricingTable .pricing-content li {
            font-size: 16px;
            font-weight: 500;
            line-height: 25px;
            padding: 0;
            margin: 0 0 13px;
            position: relative;
        }

        .pricingTable .pricing-content li:last-child {
            margin-bottom: 0;
        }

        .pricingTable .pricing-content li i {
            margin-right: 5px;
        }

        .pricingTable .pricingTable-signup a {
            color: #fff;
            background-color: #E96134;
            font-size: 18px;
            font-weight: 400;
            text-transform: uppercase;
            padding: 6px 22px;
            margin: 0 auto;
            border: none;
            border-radius: 20px;
            display: inline-block;
            transition: all 0.3s ease 0s;
        }

        .pricingTable .pricingTable-signup a:hover {
            color: #fff;
            box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.2), 0 0 0 6px rgba(0, 0, 0, 0.15);
        }

        .pricingTable.blue .title,
        .pricingTable.blue .pricingTable-signup a {
            background-color: #2AB9CB;
        }

        .pricingTable.green .title,
        .pricingTable.green .pricingTable-signup a {
            background-color: #3DA592;
        }

        @media only screen and (max-width: 990px) {
            .pricingTable {
                margin: 0 15px 40px;
            }
        }

        a:hover {
            text-decoration: none;
        }

    </style>

@endsection
