@extends('layouts.app')

@section('content')




    @if (Session::has('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Succeed</strong> {{ Session::get('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('approved'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Succeed</strong> {{ Session::get('approved') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (Session::has('acsend'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Succeed</strong> {{ session('acsend') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <!-- Example single danger button -->
    <!-- Example split danger button -->
    <div class="container ">
        <h1 class="mb-8 mt-4 text-xl float-left">Post Table</h1>

        <div class="dropdown mb-2 mr-3 mt-3 text-right float-right">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                Sort by
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                <form action="{{ route('admin.post.sort.notactive') }}" method="get"><button class="dropdown-item"
                        type="submit">Need Approval Post</button></form>
                <form action="{{ route('admin.post.sort.active') }}" method="get"><button class="dropdown-item"
                        type="submit">Approved Post</button></form>
                <form action="{{ route('admin.post.sort.ascend') }}" method="get"><button class="dropdown-item"
                        type="submit">Sort by A-Z</button></form>
                <form action="{{ route('admin.post.sort.older') }}" method="get"><button class="dropdown-item"
                        type="submit">Show Older first</button></form>



            </div>



        </div>
        @if ($posts->count())
            <table class="text-center w-full h-auto">
                <thead class="bg-black flex text-white w-full">
                    <tr class="flex w-full mb-1">
                        <th class="p-4 w-1/4">Sno</th>
                        <th class="p-4 w-1/4">Title</th>
                        <th class="p-4 w-1/4">Image</th>
                        <th class="p-4 w-1/4">Description</th>
                        <th class="p-4 w-1/4">Post By</th>
                        <th class="p-4 w-1/4">Approved</th>
                        <th class="p-4 w-1/4">Created</th>
                        <th class="p-4 w-1/4">Action</th>

                    </tr>
                </thead>
                <!-- Remove the nasty inline CSS fixed height on production and replace it with a CSS class — this is just for demonstration purposes! -->
                <tbody class="bg-grey-light flex flex-col items-center justify-between overflow-y-scroll w-full h-96">
                    <?php $i = 1; ?>
                    @foreach ($posts as $post)
                        <tr class="flex w-full mb-10">
                            <td class="p-4 w-1/4">{{ $i++ }}</td>
                            <td class="p-4 w-1/4">{{ $post->title }}</td>
                            <td class="p-4 w-1/4"><img src="{{ asset('images/' . $post->image) }}" height="30"
                                    width="80"></td>
                            <td class="p-4 w-1/4">{{ substr($post->description, 0, 100) }}</td>
                            <td class="p-4 w-1/4">{{ substr($post->user->name, 0, 100) }}</td>
                            <td class="p-4 w-1/4">
                                <form action="{{ route('admin.posts.update', $post->id) }}" method="POST">
                                    @method('put')
                                    @csrf
                                    @if ($post->status == 1)
                                        <input name="status" type="hidden" value="0">
                                        <button type="submit" class="btn btn-danger">No</button>
                                    @else
                                        <input name="status" type="hidden" value="1">
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                    @endif
                                </form>
                            </td>
                            <td class="p-4 w-1/4">{{ $post->created_at->diffForHumans() }}</td>
                            <td class="p-4 w-1/4">
                                <form action="{{ route('admin.posts.delete', $post->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="hover:underline btn btn-warning">DELETE</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h1 class="text-center mt-10">No Post</h1>
        @endif
    </div>
    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
            $("#success-alert").slideUp(500);
        });
        $(document).ready(function() {
            $("#success-alert").hide();
            $("#myWish").click(function showAlert() {
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#success-alert").slideUp(500);
                });
            });
        });
    </script>
@endsection
