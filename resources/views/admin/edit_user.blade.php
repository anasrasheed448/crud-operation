@extends('layouts.app')

@section('content')
    <header>
        <div class="h-60 bg-gray-100 flex justify-center ">
            <div class="py-6 px-8   mt-20 bg-white rounded shadow-xl ">

                @if (session('status'))
                    <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                        {{ session('status') }}
                    </div>
                @endif

                <h1 class="font-bold text-base mb-2">Edit Roles: </h1>
                <form action="{{ route('admin.users.update', $user) }}" method="post">
                    @method('put')
                    @csrf
                    @foreach ($roles as $role)
                        <div class="form-check">
                            <input type="checkbox" name="roles[]" value="{{ $role->id }}">
                            <label for="">{{ $role->name }}</label>
                        </div>
                    @endforeach
                    <button type="submit"
                        class="bg-indigo-500 text-white font-bold w-full text-center rounded">Register</button>
                </form>
            </div>
        </div>
    </header>
@endsection
