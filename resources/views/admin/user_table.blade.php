@extends('layouts.app')

@section('content')

    <div class="container">
        <h1 class="float-left mt-4 text-xl">User Table</h1>

        <a href="{{ route('admin.user.export') }}" class="float-right mt-4 mr-10 mb-2 btn btn-primary">Export</a>
        <form action="{{ route('admin.user.import') }}" class="float-right" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file" class="btn mt-4 p-0 ">
            @error('file')
                <p class="float-left font-red">{{ $message }}</p>
            @enderror
            <input type="submit" value="import" class="float-right mt-4 mr-3 btn btn-success ">
        </form>
        @if ($users->count())

            @if (Session::has('success'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Succeed</strong> {{ Session::get('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if (Session::has('acsend'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Succeed</strong> {{ session('acsend') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <table class="text-center w-full h-auto">
                <thead class="bg-black flex text-white w-full">
                    <tr class="flex w-full mb-1">
                        <th class="p-4 w-1/4">Sno</th>
                        <th class="p-4 w-1/4">Name</th>
                        <th class="p-4 w-1/4">Email</th>
                        <th class="p-4 w-1/4">Total Post</th>
                        <th class="p-4 w-1/4">Roles</th>
                        <th class="p-4 w-1/4">Created</th>
                        <th class="p-4 w-1/4">Action</th>

                    </tr>
                </thead>
                <!-- Remove the nasty inline CSS fixed height on production and replace it with a CSS class — this is just for demonstration purposes! -->
                <tbody class="bg-grey-light flex flex-col items-center justify-between overflow-y-scroll w-full h-full">
                    <?php $i = 1; ?>
                    @foreach ($users as $user)
                        <tr class="flex w-full mb-0">
                            <td class="p-4 w-1/4">{{ $i++ }}</td>
                            <td class="p-4 w-1/4">{{ $user->name }}</td>
                            <td class="p-4 w-1/4">{{ $user->email }}</td>
                            <td class="p-4 w-1/4">{{ $user->posts->count() }}</td>
                            <td class="p-4 w-1/4">
         {{ implode(', ',$user->roles()->get()->pluck('name')->toArray()) }} 
                            </td>
                            <td class="p-4 w-1/4">{{ $user->created_at->diffForHumans() }}</td>

                            <td class="p-4 w-1/4">
                                <form action="{{ route('admin.users.destroy', $user->id) }}" method="post">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="hover:underline  text-blue-900" value="DELETE">
                                </form>
                                <a class="hover:underline text-blue-900 font-bold"
                                    href="{{ route('admin.users.edit', $user->id) }}">EDIT</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <h1 class="text-center mt-10">No Post</h1>
        @endif
    </div>
    <script>
        $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
            $("#success-alert").slideUp(500);
        });
        $(document).ready(function() {
            $("#success-alert").hide();
            $("#myWish").click(function showAlert() {
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#success-alert").slideUp(500);
                });
            });
        });
    </script>
@endsection
