<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <script src="{{ asset('js/app.js') }}"></script>
    <title>Document</title>

</head>
<body>
    <nav>
        <div class="">
            <div class="flex justify-between h-16 px-10 shadow items-center">
                <div class="flex items-center space-x-8">
                    <h1 class="text-xl lg:text-2xl font-bold cursor-pointer">CRUD APP</h1>
                    <div class="hidden md:flex justify-around space-x-4">
                        <a href="/" class="hover:text-indigo-600 text-gray-700">Home</a>
                        @auth
                            <a href="{{ route('post.table') }}" class="hover:text-indigo-600 text-gray-700 ">My
                                Post</a><sup class="ml-1 mb-1 hover:text-indigo-600 text-gray-700 font-bold text-base">
                                {{ auth()->user()->posts()->count() ?? '0' }}</sup>
                            <a href="{{ route('post.create') }}" class="hover:text-indigo-600 text-gray-700">Create
                                Post</a>
                            @if(auth()->user()->hasRole('user'))
                            <a href="{{ route('payment.index') }}"
                            class="hover:text-indigo-600 text-gray-700">Subscriptions</a>
                            @endif
                        @endauth
                    </div>
                </div>

                <!-- component -->
                <!-- This is an example component -->
                <form method="get" action="{{ route('post.search') }}">
                    <div class="pt-2 relative mx-auto text-gray-600">
                        <input
                            class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none"
                            type="search" name="search" placeholder="Search">
                        <button type="submit" class="absolute right-0 top-0  mt-3 mr-4">
                            <svg class="text-gray-600 h-4 w-4 fill-current" xmlns:xlink="http://www.w3.org/1999/xlink"
                                x="0px" y="0px" viewBox="0 0 56.966 56.966"
                                style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve" width="512px"
                                height="512px">
                                <path
                                    d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                            </svg>
                        </button>
                    </div>
                </form>

                @guest
                    <div class="flex space-x-4 items-center">
                        <a href="{{ route('customLogin') }}" class="text-gray-800 text-sm">LOGIN</a>
                        <a href="{{ route('register') }}"
                            class="bg-indigo-600 px-4 py-2 rounded text-white hover:bg-indigo-500 text-sm">SIGNUP</a>
                    </div>
                @endguest
                @auth
                    @can('manage-users')
                        <div class="flex space-x-4 ">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Admin Actions
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('admin.users.index') }}">View Users</a>
                                <a class="dropdown-item" href="{{ route('admin.posts.table') }}">Posts</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    @endcan

                    <div class="flex space-x-4 items-center">
                        <h1 class="text-xl text-blue-500 lg:text-1xl font-bold cursor-pointer">{{ auth()->user()->name }}   
                        </h1>
                        <form method="post" action="{{ route('logout') }}">
                            @csrf
                            <button type="submit" class="text-gray-800 text-sm">SignOut</button>
                        </form>
                    </div>



                @endauth
            </div>
        </div>
    </nav>
    @yield('content')
</body>

</html>
