<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class Subscription_Payment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // $subscription_check = Payment::where('user_id', Auth::id())->first();
        $payment = auth()->user()->payment;
        // dd($request->user()->hasRole('admin'));
        if ($request->user()->hasRole('user')) {
            if ($payment->expiry_date <= Carbon::now())
                return redirect()->route('payment.index')->with('subscription', 'Subscription date is Over');

            if (in_array($payment->subscription_type, ['trial', 'silver', 'diamond']) && $payment->post_count == 0)
                return redirect()->route('payment.index')->with('subscription', 'Your Limit Has been reached');
        }
        return $next($request);
    }
}
