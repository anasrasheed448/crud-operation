<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use App\Models\Payment;

class PostController extends Controller
{
    public function  index()
    {
        $posts = Post::with('user','likes','comments')->where('status', 0)->orderBy('created_at', 'desc')->paginate(10);
        return  view('post.index', ['posts' => $posts]);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $posts = Post::with('user')->where('status', 0)->where('title', 'like', '%' . $search . '%')->paginate(5);
        return view('post.index', ['posts' => $posts]);
    }

    // ALL FUNCTION DOWN BELOW REQUIRES AUTHENTICATION && DEPEND ON SINGLE USER RECORDS

    public function showCreateForm()
    {
        return view('post.create');
    }

    public function store(Request  $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg|max:5048',
        ]);

        $imageName = time() . '-' . $request->title . '.' . $request->image->extension();
        $post = $request->user()->posts()->create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $imageName,
        ]);
        $request->image->move(public_path('images'), $imageName, 's3');
        $incrementPostCounter = Payment::where('user_id', $request->user()->id)->first();
        $incrementPostCounter->post_count -= 1;
        $incrementPostCounter->save();

        return redirect('/')->with('post-approval', 'Your post is submitted for admin Approval');
    }

    public function edit($id)
    {
        $user_id = Auth::id();
        $post = Post::find($id);
        if ($user_id == $post->user_id)
            return view('post.edit', ['post' => $post]);
        return back();
    }

    public function show(Post $post){
        $post->with('comments','replies');
        // dd("dd");
        return view('post.show',['post'=>$post]);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $user_id = Auth::id();
        if ($user_id == $post->user_id) {
            if ($request->image != null) {
                $imageUrl = time() . '-' . $request->name . '.' . $request->image->extension();
                $request->image->move(public_path('images'), $imageUrl, 's3');
            } else
                $imageUrl = $post->image;
            $post->update(
                [
                    'title' => $request->title,
                    'description' => $request->description,
                    'image' =>  $imageUrl,
                ]
            );
        }
        return redirect()->route('post.table');
    }


    // SHOW POST OF CURRENT AUTH USER
    public function singleUserPosts(Request $request)
    {
        $posts = $request->user()->posts()->orderBy('created_at', 'desc')->paginate(5);
        return view('post.index', ['posts' => $posts]);
    }

    public function allPost()
    {
        $posts = Post::paginate(10);
        return view('admin.posts.post_table', ['posts' => $posts]);
    }

    // USER POST TABLE DATA
    public function  tableView(Request $request)
    {
        $posts = Post::where('user_id', $request->user()->id)->orderBy('created_at', 'desc')->paginate(5);
        // $posts = $request->user()->posts()->orderBy('created_at', 'desc')->paginate(5);
        return  view('post.post_table', ['posts' => $posts]);
    }

    // USER POST TABLE DATA ASCENDING ORDER
    public function sortByAscendTable(Request $request)
    {
        $posts = Post::where('user_id', $request->user()->id)->paginate(5);
        Session::flash('acsend', 'Showing in Acsending Order');
        return  view('post.post_table', ['posts' => $posts]);
    }

    // USER POST TABLE DATA OLDER POST FIRST
    public function sortByOlderTable(Request $request)
    {
        $posts = Post::where('user_id', $request->user()->id)->orderBy('created_at')->paginate(5);
        Session::flash('success', 'showing Older first');
        return view('post.post_table', ['posts' => $posts])->with('msg', 'showing Older first');
    }

    public function destroy(Request $request, $id)
    {
        $post = Post::where('user_id', $request->user()->id)->where('id',$id);
        $post->delete();
        return back()->with('msg', 'Post Deleted');
    }
}
