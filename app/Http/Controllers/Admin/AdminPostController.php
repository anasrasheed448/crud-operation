<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use Illuminate\Support\Facades\Session;
use SebastianBergmann\Environment\Runtime;

class AdminPostController extends Controller
{
    public function  index()
    {
        $posts = Post::with('user')->paginate(10); 
        return view('admin.posts.post_table', ['posts' => $posts]);
    }

    public function update(Request $request, $id)
    {
        $post = Post::with('user')->find($id);
        $post->status = $request->status;
        $post->save();
        if($post->status == 0)
        return redirect()->back()->with('approved','Post Approved');

        return redirect()->back()->with('approved','Post Un Approved');
        
    }


    // USER POST TABLE DATA ASCENDING ORDER
    public function sortByAscendTable()
    {
        $posts = Post::with('user')->paginate(5);
        Session::flash('acsend', 'Showing in Acsending Order');
        return  view('admin.posts.post_table', ['posts' => $posts]);
    }

    // USER POST TABLE DATA OLDER POST FIRST
    public function sortByOlderTable()
    {
        $posts = Post::with('user')->orderBy('created_at')->paginate(5);
        Session::flash('success', 'showing Older first');
        return view('admin.posts.post_table', ['posts' => $posts])->with('msg', 'showing Older first');
    }
    public function showActivePost()
    {
        $posts = Post::with('user')->where('status', 0)->paginate(10);
        return view('admin.posts.post_table', ['posts' => $posts]);
    }

    public function showNotActivePost()
    {
        $posts = Post::with('user')->where('status', 1)->paginate(10);
        return view('admin.posts.post_table', ['posts' => $posts]);
    }

    public function destroy($id)
    {
        $post = Post::with('user')->find($id);
        $post->delete();
        return back()->with('msg', 'Post Deleted');
    }
}
