<?php

namespace App\Http\Controllers;

use App\Mail\WelcomeMail;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    //

    public function index(){
        return view('auth.login');
    }

    public function login(Request $request) {
        
       $fields =  $request->validate([
            'email' => 'required|string',
            'password' =>'required|string',
        ]);

        if(!Auth::attempt($request->only('email','password'))){
            return back()->with('status','invalid Credentials');
        }
        return  redirect()->route('post.index');


    }
    public function  indexRegister(){
        return view('auth.register');
    }
    public function register(Request  $request){
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string|confirmed'

        ]);
        $data = (['name'=>$fields['name'],'email'=>$fields['email']]);
        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
        ]);
        $user->roles()->attach(Role::where('name','user')->first());
        $user->payment()->create();
        Mail::to($fields['email'])->send(new WelcomeMail($data));
        return redirect()->route('login')->with('status','Account Created. Please Login');
    }

    public function  logoutUser() {
        Auth::logout();
       return redirect()->route('login');
    }
}
