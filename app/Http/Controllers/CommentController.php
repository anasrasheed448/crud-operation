<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
class CommentController extends Controller
{
    //

    public function store(Request $request,Post $post)
    {
        // dd(Auth::user()->name);
      $post->comments()->create([
          'body' => $request->body,
          'username' => Auth::user()->name,
          'parent_id' => $request->parent_id,
      ]);
        return back();
    }
}
