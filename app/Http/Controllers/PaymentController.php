<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalHttp\HttpException;

class PaymentController extends Controller
{
    public function index()
    {
        
        return view('payment.pricing');
    }
    public function silverSubscription()
    {
        $this->subscription(10,"silver");
        return redirect()->route('post.create')->with("paypal","Silver Subscription Successfull");
    }
    public function diamondSubscription()
    {
        $this->subscription(100,"diamond");
        return redirect()->route('post.create')->with("paypal","Diamond Subscription Successfull");;
    }


    public function subscription($post_count, $subscription_type)
    {
        $payment = Payment::where('user_id', Auth::id())->first();
        $payment->expiry_date = Carbon::now()->addMonth();
        $payment->post_count += $post_count;
        $payment->subscription_type = $subscription_type;
        $payment->save();
        return redirect()->route('post.create');
    }

    public function paypalCheckout($price="15")
    {
        
       
        $config = config('services.paypal');
        $environment = new SandboxEnvironment($config['client_id'], $config['client_secret']);
        $client = new PayPalHttpClient($environment);

        $request1 = new OrdersCreateRequest();
        $request1->prefer('return=representation');
        $request1->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => uniqid(),
                "amount" => [
                    "value" => $price,
                    "currency_code" => "USD"
                ]
            ]],
            "application_context" => [
                "cancel_url" => route("post.index"),
                "return_url" => $price == "15" ? route("payment.silver") : route("payment.diamond")
            ]
        ];

        try {
            $response = $client->execute($request1);
            // dd($response);
            foreach ($response->result->links as $links) {
                if ($links->rel == "approve") {
                    return redirect($links->href);
                }
            }
        } catch (HttpException $ex) {
            echo $ex->statusCode;
            dd($ex->getMessage());
        }
    }
}
